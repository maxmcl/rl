import logging
import os
import sys


logger = logging.getLogger()
handler = logging.StreamHandler(sys.stdout)
handler.setFormatter(logging.Formatter("[%(lineno)s - %(funcName)s] %(message)s"))
logger.addHandler(handler)
logger.setLevel(os.getenv("LOG_LEVEL", logging.INFO))


if __name__ == "__main__":
    logger.info('Info')
    logger.debug('Debug')
