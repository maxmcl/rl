from collections import namedtuple
from unittest.mock import MagicMock

import numpy as np
import torch

from logger import logger


Step = namedtuple("Step", ["state", "action", "reward", "next_state"])
Episode = namedtuple("Episode", ["total_reward", "steps"])


class CrossEntropy:
    """
    Cross-entropy policy learning:
        1. Sample a batch of episodes
        2. Keep the top episodes (elite episodes)
        3. Train a classifier on the (state -> action) pairs of those episodes
        4. Repeat 1->3 until reward is high enough
    """
    ENV_MAX_STEPS = 1e3

    def __init__(self, model, env, writer=MagicMock(), batch_size=16, perc_thresh=70, n_iters=1000):
        self.model = model
        self.env = env
        self.writer = writer
        self.batch_size = batch_size
        self.perc_thresh = perc_thresh  # in percent
        self.n_iters = n_iters
        self.optim = torch.optim.Adam(self.model.parameters())
        self.obj_func = torch.nn.CrossEntropyLoss()

    def get_batch(self):
        top_episodes = self._select_best_from_batch([self.get_episode() for _ in range(self.batch_size)])
        return self.to_training_data(top_episodes)

    def get_episode(self):
        is_done, state, steps, n_steps, total_reward = False, self.env.reset(), [], 0, 0
        while not is_done and (n_steps < self.ENV_MAX_STEPS):
            action = self._choose_next_action(state)
            next_state, reward, is_done, _ = env.step(action)
            steps.append(Step(state=state, action=action, reward=reward, next_state=next_state))
            state = next_state
            total_reward += reward
            n_steps += 1
        logger.debug(f'Generated episode with total_reward={total_reward} in {n_steps} steps')
        return Episode(total_reward=total_reward, steps=steps)

    def _choose_next_action(self, state):
        # Make sure state is a 2D matrix
        actions = self.model.predict_proba(torch.FloatTensor(state[None, :])).data.numpy()[0]
        return np.random.choice(np.arange(self.env.action_space.n), p=actions)

    def _select_best_from_batch(self, batch):
        logger.debug(f'Got batch {len(batch)}')
        rewards = np.array([episode.total_reward for episode in batch])
        self.mean_reward = rewards.mean()
        self.lower_bound = np.percentile(rewards, self.perc_thresh)
        top_pos = np.where(rewards >= self.lower_bound)[0]
        logger.debug(f'Reduced batch to {len(top_pos)}')
        return [batch[pos] for pos in top_pos]

    def to_training_data(self, top_episodes):
        # TODO: action is an int, state is a np array
        X = torch.FloatTensor([step.state
                               for episode in top_episodes
                               for step in episode.steps])
        y = torch.LongTensor([step.action
                              for episode in top_episodes
                              for step in episode.steps])
        return X, y

    def train(self):
        logger.info('Starting training')
        self.mean_reward, self.lower_bound, self.losses = 0, 0, []
        for it in range(self.n_iters):
            X_train, y_train = self.get_batch()
            self.model.zero_grad()
            y_pred = model.forward(X_train)
            loss = self.obj_func(y_pred, y_train)
            loss.backward()
            self.optim.step()
            self.monitor(it, loss.item())

            if self.is_solved():
                logger.info('Environment solved')
                break

    def monitor(self, it, loss):
        self.losses.append(loss)
        logger.info(f'{it} | mean_reward = {self.mean_reward:.3f} | lower_bound = {self.lower_bound:.3f} | '
                    f'loss = {self.losses[-1]:.3f}')
        self.writer.add_scalar("loss", self.losses[-1], it)
        self.writer.add_scalar("lower_bound", self.lower_bound, it)
        self.writer.add_scalar("mean_reward", self.mean_reward, it)

    def is_solved(self):
        """
        Environment considered solved if mean reward > 199
        """
        return self.mean_reward > 199


if __name__ == "__main__":
    import gym
    from torch.utils.tensorboard import SummaryWriter

    from model import Model

    logger.info('Creating env')
    env = gym.make("CartPole-v0")
    logger.info('Creating model')
    model = Model(n_hidden=24, state_space_size=env.observation_space.shape[0], action_space_size=env.action_space.n)
    logger.info('Creating solver')
    solver = CrossEntropy(model, env, writer=SummaryWriter(), batch_size=32, n_iters=10000)
    solver.train()
    torch.save(model.state_dict(), "model.trained")
    solver.writer.close()
