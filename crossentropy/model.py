import torch


class Model(torch.nn.Module):
    """
    A simple feed-forward model used to learn a (state -> action) mapping
    to learn a policy when combined with cross-entropy RL
    """

    def __init__(self, n_hidden, state_space_size, action_space_size):
        super().__init__()
        self.lin1 = torch.nn.Linear(state_space_size, n_hidden)
        self.relu = torch.nn.ReLU()
        self.lin2 = torch.nn.Linear(n_hidden, action_space_size)
        self.softmax = torch.nn.Softmax(dim=1)

    def forward(self, X):
        Z = self.relu(self.lin1(X))
        Y = self.lin2(Z)
        return Y

    def predict_proba(self, X):
        return self.softmax(self.forward(X))
